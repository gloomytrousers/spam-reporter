# TODO
* More options regarding what to do when a spam exceeds the size limit - save (as now), truncate and send anyway, or discard.
* Add additional settings to support more reporting services and other useful things such as external programs - suggestions welcome!
* Allow a [global] section in the config file, for global setting of most values. Add a 'mail_prog' setting to config file, rather than hard-coding it.
* There is a bug I can't yet track down where the input is not being split up correctly into multiple messages.
* Improvement to address munging - decode UUencoded/base64 parts and check them too.
* Improvement to address munging - find 'ROT n' versions too?
* Put some stats about the report in the report message header.
* Some messages getting dropped! Looks like it's because some spams don't terminate their last MIME part properly with `[separatorstring]--`, so following messages are appended. Need to check that MIME messages end with their terminator from the header.
