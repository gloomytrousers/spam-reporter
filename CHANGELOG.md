# Version history

## v0.1	24 Feb 2003
* Initial version

## v0.2	01 Mar 2003
* Saves spam in file if it's too big to report.
* Added limit for individual spam size (50K).
* Also munges user@host.domain based on the e-mail addresses set.
* Misc minor improvements.

## v0.3	30 Mar 2003
* Lowered message size limits, after feedback from Julian Haight.
* Now munges user and domain parts of e-mail addresses separately as well as the whole address.

## v0.4	01 Jan 2007
* Removed separate munging of user and domain parts again - it interferes with headers and leaks more information than leaving them intact.
* Now allows regular expressions for your e-mail address(es).

## v0.5	18 Feb 2007
* Changed Spam.eml filename, as apparently this is a filename used by some malware, and may cause you to get listed in various blacklists (reported by Matthew).
* Fixed a warning with spams that are too big and are written to disk instead of being reported.

## v0.5a	25 Feb 2007
* Fixed a minor warning.

## v0.6	04 Mar 2007
* Now has external configuration file in which multiple report services (e.g. SpamCop, KnujOn) can be specified.
* Now takes remaining configuration settings on command line, so there should no longer be any need to edit this script before running.
* There's now extra levels of verboseness.
* Many output messages, filenames, etc. have been changed.

## v0.7	09 Apr 2007
* Now can report to services only if a spam matches a given regular expression (useful for reporting the phishing e-mails from your spam to additional services).
* Bugfix for case where a report could be sent with no spams attached.

## v0.7a	19 May 2007
* For e-mail munging, @-subsitution changed to not allow '.', to prevent munging hostnames that aren't e-mail addresses.

## v0.7b	26 May 2007
* No code changes, just added reporting to FTC in config file.

## v0.7.3	01 Mar 2008
* No code changes, just licence as GPLv3. Changed version numbering to major.minor.revision

## v0.8.0	19 Jan 2018
* Tidying up and move to GitLab. No functional changes.
