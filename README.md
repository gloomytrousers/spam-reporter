# Spam reporter

Takes a mailbox or other file full of junk e-mails, and sends them to spam reporting services such as [SpamCop](https://www.spamcop.net/), various phishing reporting addresses if appropriate (based on content), or any other service that accepts spam reports by e-mail. Comes pre-configured with a number of reporting services which can be used out-of-the-box. Inspired by [similar scripts](https://www.spamcop.net/fom-serve/cache/166.html) on SpamCop, but much more flexible and supporting a wider range of services.

I hope you will find this useful if you get spam and you want to complain about it - that covers pretty much everyone, I think! The program is a single perl script, and should be suitable for use on most systems. I develop and use on Linux, with Sendmail/Exim, but the only system requirements are Perl and a command-line mailer (the built-in assumption is `sendmail`, and other MTAs usually provide this for compatibility). Use requires a small amount of technical knowledge, but all you really need to be able to do is to save your junk e-mail in a file then run this script. Mail system admins could best implement this by providing a folder into which users can drop spam, and running this script on the folder periodically (that's how I use it).

This is a stable and well-tested program, and any updates will be infrequent, almost to the point of non-existence, until life permits me free time again. However, if you find (or run) a spam/phishing report service that I haven't configured yet (perhaps your bank's phishing report e-mail address), submit a merge request.

## Usage

Review and amend .reporter.conf (loads of comments in the file) and stick it in your home directory. Then simply pipe a file full of spam to reporter.pl. You might want to test with a single spam and `-v` option first, just to make sure all is configured as it should be.

	$ ./reporter.pl -h
	Takes spam e-mail(s) on STDIN and reports to configured reporting services.

	Usage: ./reporter.pl [options]
	Options are:
	-c {file}	Read configuration from {file} instead of default location of
			$HOME/.reporter.conf
	-v		Be verbose. Specify once to generate some info about what's
			happening, twice to do so even if there's no mails to process,
			and three or more times for boring debugging info.
	-t		Test mode. Outputs report e-mails to file(s) instead of to the
			real mail program.
	-V		Version information.
	-h		This help.
