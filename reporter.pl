#!/usr/bin/perl -w
#
# Script to take a mailbox full of spam (or just individual messages)
# and forward them to reporting services such as Spamcop
# (http://www.spamcop.net/), or anywhere else that might be useful.
#
# Copyright (c) Russ Odom <russ@gloomytrousers.co.uk> 2018
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Instructions:
#	Before using, ensure you have installed .reporter.conf (included in the
#	archive with this script) in your home directory, and reviewed its
#	contents and edited accordingly.
#	Then, just pipe some spams to this script on STDIN. Report(s) will
#	generated and mailed to the configured services.
#	Start with -h to see some additional command line parameters.

# Setup of initial values:

# Print some info about what's happening (use -v arg to set)
#	0 = silent except when there's an error [default]
#	1 = only when we've got something to do
#	2 = even when we're given no messages
#	3+ = debug messages of increasing boringness
$verbose = 0;

# Set true to output reports to a file instead of the mail program (use -t arg to set)
$fileoutput = 0;

# The program on your system that sends mail (needs to accept mail, including headers, on STDIN)
# TODO: Make this settable in config file
$mail_program = '/usr/sbin/sendmail -oi -t';

# The config file. This is the default setting, use -c arg to change;
$configfile = "$ENV{'HOME'}/.reporter.conf";

# Program version info
$VER = '0.8.0';

# Parse command line args:
while (@ARGV) {
	$arg = shift;
	for ($arg) {
		unless (/^-[vVtch]+$/o) {die("Invalid command line argument: $_\nUse -h for help\n")}
		if (/h/o) {
			print<<END;
Takes spam e-mail(s) on STDIN and reports to configured reporting services.

Usage: $0 [options]
Options are:
-c {file}	Read configuration from {file} instead of default location of
		\$HOME/.reporter.conf
-v		Be verbose. Specify once to generate some info about what's
		happening, twice to do so even if there's no mails to process,
		and three or more times for boring debugging info.
-t		Test mode. Outputs report e-mails to file(s) instead of to the
		real mail program.
-V		Version information.
-h		This help.
END
			exit 0;
		}
		if (/V/o) {
			print<<END;
reporter.pl v$VER (c) Russ Odom 2018
Latest version available from http://www.gloomytrousers.co.uk/open_source/
END
			exit 0;
		}
		if (/v/o) {
			$verbose += (s/v//go);
			($verbose > 3) && print "Verboseness: $verbose\n";
		}
		if (/t/o) {
			$fileoutput = 1;
			($verbose > 2) && print "TEST MODE: outputting report mail(s) to file(s) instead of e-mail program\n";
		}
		if (/c/o) {
			unless (/c$/ && defined($configfile = shift)) {die("Command line argument -c must be followed by space then a filename\n")}
			($verbose > 3) && print "Using config file $configfile\n";
		}
	}
}

# Parse config file:
open(CONFIG, $configfile) || die("Can't open config file $configfile\n");
($verbose > 2) && print "Parsing config file $configfile\n";
@services = ();
$lineno = 0;
while (<CONFIG>) {
	++$lineno;

	# Skip/trim whitespace and comments
	chomp();
	s/\s*#.*$//o;
	s/^\s+|\s+$//go;
	next unless ($_);
	($verbose > 4) && print "Config line $lineno: $_\n";

	# Start a new service
	if (/^\[(.+)\]$/o) {
		$servicename = $1;
		($verbose > 2) && print "Loading settings for service '$servicename'\n";
		# TODO: Die if service name already used
		push @services, $servicename;
		next;
	}

	# A setting for the current service
	if (/^(\w+)\s*=\s*(.+)/o) {
		defined($servicename) || die("Error in config file $configfile at line $lineno: You need a [servicename] line first:\n$_\n");
		$name = $1;
		$value= $2;
		($verbose > 2) && print "\tConfig value: $name = $value\n";
		if (defined($config{"$servicename.$name"})) { die("Error in config file $configfile at line $lineno: setting '$name' already specified for service '$servicename':\n$_\n") }
		unless ($name =~ /^(active|report_to|report_from|strip_email_addresses|max_mail_size|max_spam_size|subject|text_match)$/o) { die("Error in config file $configfile at line $lineno: setting '$name' unknown:\n$_\n") }

		$config{"$servicename.$name"} = $value;
		next;
	}
	die("Error in config file $configfile at line $lineno: Invalid line:\n$_\n");
}
close(CONFIG) || die("Can't close config file $configfile\n");

# Defaults and validation
if (!@services) { die("No services configured in $configfile") }
foreach $servicename (@services) {
	# Defaults
	if (!defined $config{"$servicename.max_mail_size"}) { $config{"$servicename.max_mail_size"} = 1024*1024 }
	if (!defined $config{"$servicename.max_spam_size"}) { $config{"$servicename.max_spam_size"} = $config{"$servicename.max_mail_size"} - 1024 }
	if (!defined $config{"$servicename.subject"}) { $config{"$servicename.subject"} = 'Spam report' }
	if (!defined $config{"$servicename.text_match"}) { $config{"$servicename.text_match"} = '' }
	# Validation
	if (defined($config{"$servicename.active"})) {
		if ($config{"$servicename.active"} !~ /^([01]|REVIEW_SETTINGS_FIRST)$/o) { die("Service $servicename: setting active = $config{\"$servicename.active\"} is not an allowed value\n") }
		if ($1 eq 'REVIEW_SETTINGS_FIRST') {
			warn("You must review the settings for service $servicename before using.\nSee the config file ($configfile)\n");
			$config{"$servicename.active"} = 0;
		}
	} else {
		$config{"$servicename.active"} = 1;
	}
	if (!defined $config{"$servicename.report_to"}) { die("Service $servicename has no report_to address defined\n") }
	if ($config{"$servicename.max_mail_size"} !~ /^\d+$/ || $config{"$servicename.max_mail_size"} < 1024) { die("Service $servicename: setting max_mail_size must be a number, at least 1024\n") }
	if ($config{"$servicename.max_spam_size"} !~ /^\d+$/ || $config{"$servicename.max_spam_size"} < 1024) { die("Service $servicename: setting max_spam_size must be a number, at least 1024\n") }
	if ($config{"$servicename.max_spam_size"} > $config{"$servicename.max_mail_size"}) { die("Service $servicename: setting max_spam_size must not be bigger than max_mail_size ($config{\"$servicename.max_mail_size\"})\n") }
}

# Slurp up the input into an array of messages
@spams = ();
while (<STDIN>) {
	# Start of a new message?
	if (/^From /o or !@spams) {	# TODO: Is this a good-enough match for mail separator line?
		$spams[$#spams+1] = '';
		($verbose > 3) && print "Reading message " . ($#spams+1) . " from STDIN\n";
	} else {
		$spams[$#spams] .=  $_;
	}
}

# Remove FOLDER INTERNAL DATA message, if present
# TODO: should do for all messages not just the first, in case multiple mailboxes are piped in
if (defined($spams[0]) && ($spams[0] =~ /^Subject: DON'T DELETE THIS MESSAGE -- FOLDER INTERNAL DATA/mi)) {
	($verbose > 3) && print "FOLDER INTERNAL DATA message removed\n";
	shift(@spams);
}

# Check we got some input
if ($#spams < 0) {
	($verbose > 1) && print("No spams (on STDIN) to report!\n");
	exit;
}

# Setup of loop invariants
srand(time() ^ ($$ + ($$<<15)));
$mimeboundary = "SpamBoundary_$$" . rand(123456789);
@time = localtime();
$time[5]+=1900;
$runid = sprintf("%04d%02d%02d_%02d%02d%02d", reverse(splice(@time, 0, 6)));

# Report the spams as appropriate for each service in turn
foreach $servicename (@services) {

	if (!$config{"$servicename.active"}) {
		($verbose > 3) && print "Service $servicename is not active";
		next;
	}

	($verbose > 3) && print "Processing spams for service $servicename\n";

	# Process our user's e-mail addresses to allow more flexible matching
	if (defined($config{"$servicename.strip_email_addresses"})) {
		@email_address_patterns = split(/\s+/, $config{"$servicename.strip_email_addresses"});
		foreach $address (@email_address_patterns) {
			if ($address =~ /^\/.+\/$/) {
				# Regular expression - just remove slashes, otherwise leave as-is
				$address =~ s/^\/(.+)\/$/$1/;
			} else {
				# Convert address into a regexp (handles munging like "you**yourdomain*com")
				$address =~ s/[^a-z0-9\@]/.{0,4}/gi;

				# Be more flexible around the @, to allow for "you@host.yourdomain"
				# This handles a case I've seen where address spammed is "me@mxserver.mydomain" instead of "me@mydomain"
				$address =~ s/\@/[^.]{0,12}/g;
			}
		}
		($verbose > 4) && print "Regex patterns to strip: " . join(' ', @email_address_patterns) . "\n";
	} else {
		@email_address_patterns =  ();
	}

	$reportheader = (defined($config{"$servicename.report_from"})?"From: $config{\"$servicename.report_from\"}\n":'');
	$reportheader .= <<END;
To: $config{"$servicename.report_to"}
Subject: $config{"$servicename.subject"}
X-Reported-With: Reported with reporter.pl v$VER
	(http://www.gloomytrousers.co.uk/open_source/)
MIME-Version: 1.0
Content-Type: multipart/mixed;
	boundary="$mimeboundary"

END

	# Loop over our spams, attaching to the report(s) as we go
	@reports = ('');
	for ($i=0; $i <= $#spams; $i++) {

		($verbose > 3) && print "Processing spam $i\n";

		if ($spams[$i] =~ /^Subject: (.*)/m) {
			$subject = $1;
			$subject =~ s/\n/\\n/g;
		} else {
			$subject = '[No subject]';
		}

		# Check we're OK to report to this service based on content
		if ($config{"$servicename.text_match"} && eval("\$spams[\$i] !~ /$config{\"$servicename.text_match\"}/ims")) {
		    $verbose && print("$subject: Text doesn't match pattern for reporting to $servicename - skipping\n");
		    next;
		}

		# Check that the spam is not too big to report
		if (length($spams[$i]) > $config{"$servicename.max_spam_size"}) {
			warn("WARNING: This spam too big to report to $servicename (limit is $config{\"$servicename.max_spam_size\"} bytes):\n\t" . length($spams[$i]) . " bytes, subject: $subject\n");
			# TODO: allow for actions: save, truncate (but be careful to close mime boundary?), discard
			$tempname = "reporter_${runid}_${servicename}_toobig_$i.tmp.eml";
			if (open(TEMP, ">$tempname")) {
				print TEMP munge($spams[$i], @email_address_patterns);
				close(TEMP);
				warn("\tSaved as '$tempname' for manual review\n");
			} else {
				warn("\tCouldn't save '$tempname' - mail lost\n");
			}
			next;
		}

		# Check that we've not exceeded the max size of this report, and start a new one if so
		if (length($reports[$#reports]) + length($spams[$i]) > $config{"$servicename.max_mail_size"}) {
			$verbose && print("Exceeded max size ($config{\"$servicename.max_mail_size\"} bytes) for report " . ($#reports+1) . " to $servicename; starting a new report e-mail\n");
			$reports[$#reports+1] = '';
		}

		# Attach spam to the current report
		$verbose && print "Attaching: $subject\n";
		$mimeheader = <<END;
--$mimeboundary
Content-Type: message/rfc822;
	name="Spam"
Content-Disposition: attachment;
	filename="spam_report_$#{reports}_$i.eml"

END

		$reports[$#reports] .= $mimeheader . munge($spams[$i], @email_address_patterns) . "\n";
	}

	# Send the report(s)
	for ($i = 0; $i <= $#reports; $i++) {
		next unless ($reports[$i]);	# There are a couple of cases that can result in empty report mails; skip 'em
		if ($fileoutput) {
			open(SENDMAIL, ">reporter_${runid}_${servicename}_report_$i.eml") or die("Can't open output file\n");
		} else {
			open(SENDMAIL, "|$mail_program") or die("Can't open mail program $mail_program\n");
		}
		print SENDMAIL "$reportheader$reports[$i]\n--$mimeboundary--\n";
		close(SENDMAIL);
		$verbose && print "Report " . ($i+1) . " sent to service $servicename.\n";
	}
}
($verbose > 2) && print "All done!\n";


# Munges sensitive values (to foil web bugs, etc.) before reporting the spam
sub munge {
	my $spam = shift;
	my $mungecount = 0;
	foreach $address (@_) {
		eval("\$mungecount += (\$spam =~ s/$address/[EMAIL ADDRESS REMOVED]/gis)");
	}
	$mungecount && ($verbose > 3) && print "\t$mungecount occurrence(s) of your e-mail address(es) removed\n";
	return $spam;

	# TODO: Some reports in news:spamcop.help about addresses being munged with ROT n. Could ROT [1-25] all alphanumerics and generate a much larger set to test.
	# Note: Case-sensitivity for ROT n versions? We should use an all-lowercase version of the mail to find the positions of matches, and replace them in the actual message using that index.
}
